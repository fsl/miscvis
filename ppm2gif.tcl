#   Copyright (C) 2012 University of Oxford
#
#   TCLCOPYRIGHT

set graphpic [ image create photo -file [ file rootname $argv ].ppm ]
$graphpic write  [ file rootname $argv ].gif -format gif
exit
   
